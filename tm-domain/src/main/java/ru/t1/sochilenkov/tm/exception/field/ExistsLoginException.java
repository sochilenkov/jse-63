package ru.t1.sochilenkov.tm.exception.field;

public final class ExistsLoginException extends AbstractFieldException {

    public ExistsLoginException() {
        super("Error! This login already exists...");
    }

}
